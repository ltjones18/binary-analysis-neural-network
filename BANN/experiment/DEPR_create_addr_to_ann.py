import pickle
import editdistance
from idaapi import *
from idautils import *
from idc import *
import basic_block
require("basic_block")

BATCH_MODE = False
VERIFY = False
MATCH_BBS = False
GET_DICT = False

filename = GetInputFile()
ann_filename = filename[:-3]
ann_filename += "ann.elf"
#SAVE_FILE = "guide/"+ann_filename+".ann_bb_guide.pylist"
SAVE_FILE = "C:\\Users\\student\\Documents\\ChthonianCyberServices\\ml-dev\\BANN\\experiment\\Chimera\\bcfloop1\\guide\\" + ann_filename+".ann_bb_guide.pylist"
#DICT_FILE = "dict/"+filename + ".addr_to_ann.pydict"
DICT_FILE = "C:\\Users\\student\\Documents\\ChthonianCyberServices\\ml-dev\\BANN\\experiment\\Chimera\\bcfloop1\\dict\\"+filename + ".addr_to_ann.pydict"


def find_minimums(l):
    m = sys.maxint
    indices = list()
    for i in range(0, len(l)):
        item = l[i]
        if item < m:
            m = item
            indices = [i]
        elif item == m:
            indices.append(i)
    return m,indices


if __name__ == '__main__':
    print("Starting to create addr to annotation dictionary")
    print("Guide: %s" % SAVE_FILE)
    print("Dict: %s" % DICT_FILE)
    autoWait()

    with open(SAVE_FILE, 'rb') as f:
        ann_bb_guide = pickle.load(f)

        textSel = SegByName(".text")
        textEa = SegByBase(textSel)

        unann_bb = list()
        # create first bb
        new_bb = basic_block.BasicBlockSeq(0, textEa)#, None)
        unann_bb.append(new_bb)

        #TODO: create unann
        #Match with ann_bb_guide
        #set annotation flags
        end_bb = False
        for h_ea in Heads(SegStart(textEa), SegEnd(textEa)):
            if end_bb:
                end_bb = False
                old_bb = new_bb
                new_bb = None
                old_bb.addrs = range(old_bb.start_addr, h_ea)
                for h_bb in Heads(old_bb.addrs[0], old_bb.addrs[-1]):
                    mnem = GetMnem(h_bb)
                    #Trying to filter out the mnemonics that cause differences, but this is not enough
                    if mnem != 'pop' and mnem != 'push' and mnem != "lea" and mnem != "mov":
                        old_bb.opcodes += mnem

            if isData(GetFlags(h_ea)) or GetFunctionFlags(h_ea) & FUNC_LIB != 0:
                continue

            if new_bb is None:
                new_bb = basic_block.BasicBlockSeq(len(unann_bb), h_ea)#, old_bb)
                unann_bb.append(new_bb)

            ua_ana0(h_ea)
            if ida_idp.is_basic_block_end(False):
                end_bb = True
        #TODO: include this in create_ann_bb_guide, or share code somehow??
        #finishing last basic block if unfinished
        if new_bb.addrs == None:
            new_bb.addrs = range(new_bb.start_addr, SegEnd(textEa))
            for h_bb in Heads(new_bb.addrs[0], new_bb.addrs[-1]):
                mnem = GetMnem(h_bb)
                #Trying to filter out the mnemonics that cause differences, but this is not enough
                if mnem != 'pop' and mnem != 'push' and mnem != "lea" and mnem != "mov":
                    old_bb.opcodes += mnem + "\n"
    print("Guide BBs = %d, Verify BBs = %d" % (len(ann_bb_guide), len(unann_bb)))

    if MATCH_BBS:
        print("Brute force comparing unann_bb and ann_bb_guide...")
        matrix = list()
        for guide_bb in ann_bb_guide:
            matrix.append(list())
            matrix[guide_bb.index] = list()
            for bb in unann_bb:
                matrix[guide_bb.index].append(editdistance.eval(guide_bb.opcodes, bb.opcodes))

        print("adjacent values")
        diag = min(len(unann_bb), len(ann_bb_guide))
        for i in range(0, diag):
            print(matrix[i][i])

        with open("matrix.txt", 'w') as f:
        #print(matrix)
            f.writelines('\t'.join(str(j) for j in i) + '\n' for i in matrix)

        for guide_bb in ann_bb_guide:
            m,indices = find_minimums(matrix[guide_bb.index])
            if len(indices) > 1:
                print("%d: Multiple mins: %d @ %s" % (guide_bb.index,m,str(indices)))

    length = min(len(ann_bb_guide), len(unann_bb))
    for x in range(0, length):
        ed = editdistance.eval(ann_bb_guide[x].opcodes, unann_bb[x].opcodes)
        if ed != 0:
            print("%d: bbs different by %d, ann@%x unann@%x" % (x,ed,ann_bb_guide[x].start_addr,unann_bb[x].start_addr))

    if GET_DICT:
        addr_to_ann = dict()
        for bb in unann_bb:
            if bb.annotated != basic_block.ANN_NONE:
                for addr in bb.addrs:
                    addr_to_ann[addr] = bb.annotated

        with open(DICT_FILE, "wb") as f:
            pickle.dump(addr_to_ann, f)

    print("Finished")
    if BATCH_MODE:
        Exit(0)