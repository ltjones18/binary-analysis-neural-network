# Big Lists, Unmemoryzed and Fairly Flat
# Object for keeping track of linear basic blocks via database
import collections
import sqlite3

class Bbs(collections.MutableSequence):
    def __init__(self, pathtobin, pathtodb, buffer_size):
        self._buffer_size = buffer_size
        self._dbconn = sqlite3.connect(pathtodb)
        self._dbcur = self._dbconn.cursor()
        self._path = pathtobin
        self._list = list()     #transparent, scrolling list backed by database
        self._list_start = None #db relative index of first list item
        self._list_end = None   #db relative index of last list item

    def __len__(self):
        t = (self._path,)
        self._dbcur.execute("SELECT COUNT(*) from bbs where path LIKE ?", t)
        result = self._dbcur.fetchone()
        return result[0]

    def __delitem__(self, index):
        pass

    def __getitem__(self, index):
        pass

    def __setitem__(self, key, value):
        pass

    def insert(self, index, value):
        pass