import sqlite3

DEBUG = True

if __name__ == "__main__":
    conn = sqlite3.connect('bbs.db')
    c = conn.cursor()
    #create table
    #c.execute('''CREATE TABLE bbs
    #             (path text, bb_index integer, annotated integer, start_addr integer, end_addr integer, opcodes text,
    #             func text, h_last integer, end_mnem text, bitshred blob)''')

    if DEBUG:
        #c.execute('''INSERT INTO bbs VALUES ('Chimera/bcfloop1/g/brubeck.o0.bcf.ann.g.elf', 0, 0, 123, 125,
        #            'add REG REF', 'main', 123, 'add', 'sadasffasd')''')

        #c.execute('''INSERT INTO bbs VALUES ('Chimera/bcfloop1/g/brubeck.o0.bcf.ann.g.elf', 1, 1, 126, 167,
        #            'add REG REF; pop; push; nop', 'main', 163, 'nop', 'asdfasgasdg')''')

        c.execute('''INSERT INTO bbs VALUES ('Chimera/bcfloop1/g/brubeck.o0.bcf.ann.g.elf', 2, 2, 168, 211,
                    'sub REG REG; CALL NEAR; add IMM REG', 'main', 209, 'add', 'asdfgadlkjf')''')

        conn.commit()

        conn.close()