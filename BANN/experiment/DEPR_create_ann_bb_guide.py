import pickle
from idaapi import *
from idautils import *
from idc import *
import basic_block
require("basic_block")

BATCH_MODE = True

SEARCH_DONE = 11
FIRST_OPND = 0
SECOND_OPND = 1
LARGE_BASE = 0x1000  # ltj: using a large base forces IDA to stop using alias for location

#TODO: create filename from binary name
filename = GetInputFile()
#filename = filename[:-7]
SAVE_FILE = "guide/"+filename+".ann_bb_guide.pylist"


def get_c_str_at_offset(addr):
    c_str = ""
    b = Byte(addr)
    i = 0
    while b != 0:
        i += 1
        c_str += chr(b)
        b = Byte(addr+i)

    return c_str

def get_opcodes(start_addr, end_addr):
    opcodes = ""


if __name__ == '__main__':
    print("Starting to create annotated basic block guide")
    autoWait()
    textSel = SegByName(".text")
    textEa = SegByBase(textSel)

    ann_bb_guide = list()
    # create first bb
    new_bb = basic_block.BasicBlockSeq(0, textEa)#, None)
    ann_bb_guide.append(new_bb)

    origannstr = ""
    altannstr = ""
    segformat = re.compile("^cs:[0-9A-Fa-f]+h$")
    segoff = ""
    end_bb = False
    i_bb = 0
    for h_ea in Heads(SegStart(textEa), SegEnd(textEa)):
        if end_bb:
            end_bb = False
            old_bb = new_bb
            new_bb = None
            old_bb.addrs = range(old_bb.start_addr, h_ea)
            for h_bb in Heads(old_bb.addrs[0], old_bb.addrs[-1]):
                mnem = GetMnem(h_bb)
                #ignore these stack manipulators because the stack changes between ann and unann
                if mnem != 'pop' and mnem != 'push' and mnem != "lea" and mnem != "mov":
                    old_bb.opcodes += mnem
            if old_bb.annotated != basic_block.ANN_ORIGINAL and old_bb.annotated != basic_block.ANN_ALTERED:
                old_bb.annotated = basic_block.ANN_NONE
            i_bb = 0

        if isData(GetFlags(h_ea)) or GetFunctionFlags(h_ea) & FUNC_LIB != 0:
            continue

        if new_bb is None:
            new_bb = basic_block.BasicBlockSeq(len(ann_bb_guide), h_ea)#, old_bb)
            ann_bb_guide.append(new_bb)

        if i_bb < 10:
            if GetMnem(h_ea) == "lea":
                strname = GetOpnd(h_ea, SECOND_OPND)
                if strname == origannstr:
                    new_bb.annotated = basic_block.ANN_ORIGINAL
                    i_bb = SEARCH_DONE
                if strname == altannstr:
                    new_bb.annotated = basic_block.ANN_ALTERED
                    i_bb = SEARCH_DONE
                OpOff(h_ea, SECOND_OPND, LARGE_BASE) #convert to offset
                segoff = GetOpnd(h_ea, SECOND_OPND)
            if GetMnem(h_ea) == "call":
                if GetOpnd(h_ea, FIRST_OPND) == "printf":
                    if segoff == "":
                        raise ValueError("call to printf found in basic block prologue, but no previous"
                                         " Segment Offset found.")
                    elif not segformat.match(segoff):
                        raise ValueError("Segment offset in unexpected format: "+segoff)
                    else:
                        offset = segoff.split(":")[1]
                        offset = offset[:-1]
                        c_str = get_c_str_at_offset(int(offset, 16))
                        #print("c_str: %s" % c_str)
                        if c_str == "originalBBStartAnnotation":
                            origannstr = strname
                            new_bb.annotated = basic_block.ANN_ORIGINAL
                            i_bb = SEARCH_DONE
                        elif c_str == "alteredBBStartAnnotation":
                            altannstr = strname
                            new_bb.annotated = basic_block.ANN_ALTERED
                            i_bb = SEARCH_DONE
            i_bb += 1

        ua_ana0(h_ea)
        if is_basic_block_end(False):
            end_bb = True

    if new_bb is not None:
        if new_bb.annotated != basic_block.ANN_ORIGINAL or new_bb.annotated != basic_block.ANN_ALTERED:
            new_bb.annotated = basic_block.ANN_NONE

    #for bb in ann_bb_guide:
    #    print("%d: %d,%x" % (bb.index,bb.annotated,bb.start_addr))

    with open(SAVE_FILE, 'wb') as f:
        pickle.dump(ann_bb_guide, f)

    print("%s saved" % SAVE_FILE)

    print("Finished")
    if BATCH_MODE:
        Exit(0)