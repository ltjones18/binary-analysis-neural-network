#!/bin/bash
#TODO: add file checks to make sure step works
#TODO: move first echo to only if step is invoked

bin=$1
binann=$2
num=$3

if [ -s $binann.unann.bbs ]
then
	echo "$binann.unann.bbs already exists"
else
	echo "Creating basic block list out of $binann"
	idaw64 -A -S"./bbify.py $binann.unann.bbs" $binann
	if [ -s $binann.unann.bbs ]
	then
		:
	else
		echo "Creating $binann.unann.bbs failed"
		exit -1
	fi
fi

if [ -s $bin.unann.bbs ]
then
	echo "$bin.unann.bbs already exists"
else
	echo "Creating basic block list out of $bin"
	idaw64 -A -S"./bbify.py $bin.unann.bbs" $bin
	if [ -s $bin.unann.bbs ]
	then
		:
	else
		echo "Creating $bin.unann.bbs failed"
		exit -1
	fi
fi

if [ -s $binann.ann.bbs ]
then
	echo "$binann.ann.bbs already exists"
else
	echo "Annotating basic block list with $binann"
	idaw64 -A -S"./annotate_bbs.py $binann.unann.bbs $binann.ann.bbs" $binann
	if [ -s $binann.ann.bbs ]
	then
		:
	else
		echo "Creating $binann.ann.bbs failed"
		exit -1
	fi
fi

#if [ -s $bin.verify.txt ]
#then
#	echo "$bin.verify.txt already exists"
#else
#	echo "Printing possible discrepant basic blocks to $bin.verify.txt"
#	python verify_bb_order.py $binann.ann.bbs $bin.unann.bbs > $bin.verify.txt
#	if [ -s $bin.verify.txt ]
#	then
#		:
#	else
#		echo "Creating $bin.verify.txt failed"
#		exit -1
#	fi
#fi

if [ -s $binann.dict ]
then
	echo "$binann.dict already exists"
else
	echo "Making address to annotation dictionary"
	python create_annotated_addrs.py $binann.ann.bbs $bin.unann.bbs $binann.dict
	if [ -s $binann.dict ]
	then
		:
	else
		echo "Creating $binann.dict failed"
		exit -1
	fi
fi

echo "Creating csvs out of $bin"
python create_obf_csv.py $bin $binann.dict $(dirname $bin)/ $num
