import org.datavec.api.records.reader.SequenceRecordReader;
import org.datavec.api.records.reader.impl.csv.CSVSequenceRecordReader;
import org.datavec.api.split.NumberedFileInputSplit;
import org.deeplearning4j.datasets.datavec.SequenceRecordReaderDataSetIterator;
import org.deeplearning4j.datasets.iterator.AsyncDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.Layer;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.*;
import org.deeplearning4j.nn.conf.layers.GravesBidirectionalLSTM;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.PerformanceListener;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.ExistingMiniBatchDataSetIterator;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class BANN {
    private static final Logger log = LoggerFactory.getLogger(BANN.class);
    private static final int NUM_LABELS = 2;
    private static final int prefetchSize = 10;
    private static final int minibatchSize = 32;

    public static void main(String[] args){
        //train_new_network();

        //for( int i = 0; i <= 107; i++){
         //   load_and_run_network("NNs/Chimera-short-batch-bcf-random/BANN-BCF-NEW-0.zip", i, "experiment/Chimera/bcfloop1/g/pred/");
        //}
        for( int i = 0; i <=734; i++) {
            load_and_run_network_2lbl("NNs/Paper/datacomphi-2lbl-mal-bi/BANN-BCF-FULL-0.zip", i, "experiment/Chimera/ollvm-all/mal/pred/");
        }
        //load_and_run_network_2lbl("NNs/Paper/datacomplow-2lbl-nomal-bi/BANN-BCF-NEW-0.zip", 150, "experiment/Chimera/ollvm-all/mal/pred_craft/");


//        try {
//            presave_kfold_data(10, 0, 9499, "experiment/Chimera/bcfloop1/g/data/",
//                    "experiment/Chimera/bcfloop1/g/label/", "experiment/Chimera/bcfloop1/g/data/",
//                    "experiment/Chimera/bcfloop1/g/label/", "experiment/Chimera/bcfloop1/g/data/presave-train-%d-data/",
//                    "experiment/Chimera/bcfloop1/g/data/presave-test-%d-data/");
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }

        //train_new_network_on_presaved();

        //load_and_run_network_step("NNs/Chimera-short-batch-bcf-random/BANN-BCF-NEW-0.zip", 0);
    }

    public static void presave_kfold_data(int k, int ds_min, int ds_max, String csvs_train_data_folder,
                                          String csvs_train_labels_folder, String csvs_test_data_folder,
                                          String csvs_test_labels_folder, String nth_train_kfold_folder,
                                          String nth_test_kfold_folder) throws Exception{
        //int k = 10;
        //int ds_min = 0;
        //int ds_max = 9499;
        int ds_size = ds_max - ds_min + 1;
        if (ds_size % k != 0){
            throw new Exception("Data set not divisible by k");
        }
        int kfold_size = ds_size / k;
        int test_lidx = 0;
        int test_ridx = 0;
        int ltrain_lidx = -1;
        int ltrain_ridx = -1;
        int rtrain_lidx = -1;
        int rtrain_ridx = -1;

        for (int i = 0; i < k; i++) {

            // ----- Configure the data (k-fold cross validation) -----
            test_lidx = i * kfold_size;
            test_ridx = test_lidx + kfold_size - 1;

            if (test_lidx - 1 < ds_min) {
                ltrain_lidx = -1;
                ltrain_ridx = -1;
            } else {
                ltrain_lidx = ds_min;
                ltrain_ridx = test_lidx - 1;
            }

            if (test_ridx + 1 > ds_max) {
                rtrain_lidx = -1;
                rtrain_ridx = -1;
            } else {
                rtrain_lidx = test_ridx + 1;
                rtrain_ridx = ds_max;
            }

            SequenceRecordReader testFeatures = new CSVSequenceRecordReader();
            testFeatures.initialize(new NumberedFileInputSplit(
                    csvs_test_data_folder + "%d.csv", test_lidx, test_ridx));
            SequenceRecordReader testLabels = new CSVSequenceRecordReader();
            testLabels.initialize(new NumberedFileInputSplit(
                    csvs_test_labels_folder + "%d.csv", test_lidx, test_ridx));

            DataSetIterator testData = new SequenceRecordReaderDataSetIterator(testFeatures, testLabels, minibatchSize, NUM_LABELS,
                    false, SequenceRecordReaderDataSetIterator.AlignmentMode.ALIGN_END);

            SequenceRecordReader trainFeatures = new CSVSequenceRecordReader();
            trainFeatures.initialize(new NumberedFileInputNonContiguousSplit(
                    csvs_train_data_folder + "%d.csv", ltrain_lidx, ltrain_ridx, rtrain_lidx, rtrain_ridx));
            SequenceRecordReader trainLabels = new CSVSequenceRecordReader();
            trainLabels.initialize(new NumberedFileInputNonContiguousSplit(
                    csvs_train_labels_folder + "%d.csv", ltrain_lidx, ltrain_ridx, rtrain_lidx, rtrain_ridx));

            DataSetIterator trainData = new SequenceRecordReaderDataSetIterator(trainFeatures, trainLabels, minibatchSize, NUM_LABELS,
                    false, SequenceRecordReaderDataSetIterator.AlignmentMode.ALIGN_END);

            File trainFolder = new File(String.format(nth_train_kfold_folder, i));
            trainFolder.mkdirs();

            File testFolder = new File(String.format(nth_test_kfold_folder, i));
            testFolder.mkdirs();

            int trainDataSaved = 0;
            int testDataSaved = 0;
            while(trainData.hasNext()) {
                trainData.next().save(new File(trainFolder,"mb" + minibatchSize + "-train-" + trainDataSaved + ".bin"));
                trainDataSaved++;
            }

            while(testData.hasNext()) {
                testData.next().save(new File(testFolder,"mb" + minibatchSize + "-test-" + testDataSaved + ".bin"));
                testDataSaved++;
            }
        }
    }

    public static void load_and_run_network_step(String nn_path, int data_num){//{, String preds_path) {
        try {
            //PrintWriter pred_file = new PrintWriter(preds_path + data_num, "UTF-8");

            MultiLayerNetwork net = ModelSerializer.restoreMultiLayerNetwork(nn_path);

            SequenceRecordReader testFeatures = new CSVSequenceRecordReader();
            testFeatures.initialize(new NumberedFileInputSplit(
                    "experiment/Chimera/bcfloop1/g/data/%d.csv", data_num, data_num));
            SequenceRecordReader testLabels = new CSVSequenceRecordReader();
            testLabels.initialize(new NumberedFileInputSplit(
                    "experiment/Chimera/bcfloop1/g/label/%d.csv", data_num, data_num));

            DataSetIterator testData = new SequenceRecordReaderDataSetIterator(testFeatures, testLabels, 1, NUM_LABELS,
                    false, SequenceRecordReaderDataSetIterator.AlignmentMode.ALIGN_END);

            while (testData.hasNext()) {
                //System.out.println("data!");
                DataSet ds = testData.next();
                //if( ds != null ) {
                //    System.out.println("ds not null!");
                //}

                INDArray fm = ds.getFeatureMatrix();
                //System.out.print(fm);
                //if( fm != null ){
                //    System.out.println("fm not null!");
                //}

                //TODO: get single one-hot encoding for one timestep
                INDArray col = fm.getColumn(0);
                INDArray row = fm.getRow(0).getColumn(0);
                //INDArray in = Nd4j.create(1,256);
                row.setShape(1,256);

                System.out.print(row);

                //TODO: create new function for getting the hidden state at each time step
                // Perhaps net.rnnTimeStep(sliceOfSequence); net.getLayer(0)
                //@syreal17 weights will be in the layer parameters
                //you'll have to iterate through each param index using something like .getParam('0_b')
                net.rnnTimeStep(row);


                //INDArray output = net.output(fm);
            }

        } catch (IOException | InterruptedException e){
            System.out.print(e.getMessage());
        }
    }

    public static void load_and_run_network(String nn_path, int data_num, String preds_path){
        try {
            PrintWriter pred_file = new PrintWriter(preds_path + data_num, "UTF-8");

            MultiLayerNetwork net = ModelSerializer.restoreMultiLayerNetwork(nn_path);

            SequenceRecordReader testFeatures = new CSVSequenceRecordReader();
            testFeatures.initialize(new NumberedFileInputSplit(
                    "experiment/Chimera/ollvm-all/mal/data-low-2lbl-dup-chunk-contig/%d.csv", data_num, data_num));
            SequenceRecordReader testLabels = new CSVSequenceRecordReader();
            testLabels.initialize(new NumberedFileInputSplit(
                    "experiment/Chimera/ollvm-all/mal/label-low-2lbl-dup-chunk-contig/%d.csv", data_num, data_num));

            DataSetIterator testData = new SequenceRecordReaderDataSetIterator(testFeatures, testLabels, 1, NUM_LABELS,
                    false, SequenceRecordReaderDataSetIterator.AlignmentMode.ALIGN_END);

            while( testData.hasNext() ) {
                //System.out.println("data!");
                DataSet ds = testData.next();
                //if( ds != null ) {
                //    System.out.println("ds not null!");
                //}

                INDArray fm = ds.getFeatureMatrix();
                //System.out.print(fm);
                //if( fm != null ){
                //    System.out.println("fm not null!");
                //}

                //TODO: create new function for getting the hidden state at each time step
                // Perhaps net.rnnTimeStep(sliceOfSequence); net.getLayer(0)
                //@syreal17 weights will be in the layer parameters
                //you'll have to iterate through each param index using something like .getParam('0_b')


                INDArray output = net.output(fm);
                //System.out.print(output);

                for( int i=0; i<output.size(2); i++){
                    System.out.println(i);
                    double class0pred = output.getDouble(0,0,i);
                    double class1pred = output.getDouble(0,1,i);
                    double class2pred = output.getDouble(0,2,i);
                    double max = Math.max(class0pred, class1pred);
                    max = Math.max(max,class2pred);
                    String classpred = "";

                    if(max == class0pred && max == class1pred && class2pred == max){
                        classpred = "012Tie";
                    } else if (max == class0pred && max == class1pred){
                        classpred = "01Tie";
                    } else if (max == class0pred && max == class2pred){
                        classpred = "02Tie";
                    } else if (max == class1pred && max == class2pred){
                        classpred = "12Tie";
                    } else if (max == class0pred){
                        classpred = "0";
                    } else if (max == class1pred){
                        classpred = "1";
                    } else if (max == class2pred){
                        classpred = "2";
                    } else {
                        classpred = "error!";
                    }

                    String class0pred_s = String.valueOf(class0pred);
                    String class1pred_s = String.valueOf(class1pred);
                    String class2pred_s = String.valueOf(class2pred);
                    String max_s = String.valueOf(max);

                    pred_file.println(class0pred_s + ", " + class1pred_s + ", " + class2pred_s + ",," + max_s + ", " + classpred);
                }

                pred_file.close();
            }

        } catch (IOException | InterruptedException e){
            System.out.print(e.getMessage());
        }

    }

    public static void load_and_run_network_2lbl(String nn_path, int data_num, String preds_path){
        try {
            PrintWriter pred_file = new PrintWriter(preds_path + data_num, "UTF-8");

            MultiLayerNetwork net = ModelSerializer.restoreMultiLayerNetwork(nn_path);

            SequenceRecordReader testFeatures = new CSVSequenceRecordReader();
            testFeatures.initialize(new NumberedFileInputSplit(
                    "experiment/Chimera/ollvm-all/mal/data-beanstalkd_test4/%d.csv", data_num, data_num));
            SequenceRecordReader testLabels = new CSVSequenceRecordReader();
            testLabels.initialize(new NumberedFileInputSplit(
                    "experiment/Chimera/ollvm-all/mal/label-beanstalkd_test4/%d.csv", data_num, data_num));

            DataSetIterator testData = new SequenceRecordReaderDataSetIterator(testFeatures, testLabels, 1, NUM_LABELS,
                    false, SequenceRecordReaderDataSetIterator.AlignmentMode.ALIGN_END);

            while( testData.hasNext() ) {
                //System.out.println("data!");
                DataSet ds = testData.next();
                //if( ds != null ) {
                //    System.out.println("ds not null!");
                //}

                INDArray fm = ds.getFeatureMatrix();
                //System.out.print(fm);
                //if( fm != null ){
                //    System.out.println("fm not null!");
                //}

                //TODO: create new function for getting the hidden state at each time step
                // Perhaps net.rnnTimeStep(sliceOfSequence); net.getLayer(0)
                //@syreal17 weights will be in the layer parameters
                //you'll have to iterate through each param index using something like .getParam('0_b')


                INDArray output = net.output(fm);
                //System.out.print(output);

                for( int i=0; i<output.size(2); i++){
                    //System.out.println(i);
                    double class0pred = output.getDouble(0,0,i);
                    double class1pred = output.getDouble(0,1,i);
                    double max = Math.max(class0pred, class1pred);
                    String classpred = "";

                    if (max == class0pred && max == class1pred){
                        classpred = "01Tie";
                    } else if (max == class0pred){
                        classpred = "0";
                    } else if (max == class1pred){
                        classpred = "1";
                    } else {
                        classpred = "error!";
                    }

                    String class0pred_s = String.valueOf(class0pred);
                    String class1pred_s = String.valueOf(class1pred);
                    String max_s = String.valueOf(max);

                    pred_file.println(class0pred_s + ", " + class1pred_s + ", " + ",," + max_s + ", " + classpred);
                }

                pred_file.close();
            }

        } catch (IOException | InterruptedException e){
            System.out.print(e.getMessage());
        }

    }

    public static void train_new_network_on_presaved(){
        try {

            // ----- Configure the network -----
            MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                    //.seed(123)    //Random number generator seed for improved repeatability. Optional.
                    .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).iterations(1)
                    .weightInit(WeightInit.XAVIER)
                    .updater(Updater.RMSPROP)
                    .learningRate(0.1)
                    //.lrPolicyDecayRate(1)
                    //.lrPolicyPower(0.5)
                    //.learningRateDecayPolicy(LearningRatePolicy.Inverse)
                    //.gradientNormalization(GradientNormalization.ClipElementWiseAbsoluteValue)
                    //.gradientNormalizationThreshold(0.5)
                    .list()
                    .layer(0, new GravesBidirectionalLSTM.Builder().activation(Activation.TANH).nIn(256).nOut(16).build())
                    .layer(1, new RnnOutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                            .activation(Activation.SOFTMAX).nIn(16).nOut(NUM_LABELS).build())
                    .pretrain(false).backprop(true).build();

            int k = 10;
            int ds_min = 0;
            int ds_max = 9499;
            int ds_size = ds_max - ds_min + 1;
            if (ds_size % k != 0){
                throw new Exception("Data set not divisible by k");
            }
            String[] results = new String[k];
            // ----- Train the network, evaluating the test set performance at each epoch -----
            String str = "Test set evaluation at kfold %d, epoch %d: Accuracy = %.2f, F1 = %.2f, R = %.2f, P = %.2f";
            for (int i = 0; i < 4; i++) {
                MultiLayerNetwork net = new MultiLayerNetwork(conf);
                net.init();
                //net.setListeners(new ScoreIterationListener(20));
                net.setListeners(new PerformanceListener(20));

                // ----- Configure the data (k-fold cross validation) -----
                DataSetIterator existingTrainingData = new ExistingMiniBatchDataSetIterator(
                        new File("experiment/Chimera/bcfloop1/g/data/presave-train-"+i+"-data"),"mb95-train-%d.bin");
                DataSetIterator asyncTrain = new AsyncDataSetIterator(existingTrainingData);
                DataSetIterator existingTestData = new ExistingMiniBatchDataSetIterator(
                        new File("experiment/Chimera/bcfloop1/g/data/presave-test-"+i+"-data"),"mb95-test-%d.bin");
                DataSetIterator asyncTest = new AsyncDataSetIterator(existingTestData);

                System.out.println("----------------------------------------");

                int nEpochs = 3;
                for (int j = 0; j < nEpochs; j++) {
                    System.out.println("Epoch: " + j + "; K-Fold: " + i);
                    net.fit(asyncTrain);

                    //Evaluate on the test set:
                    Evaluation evaluation = net.evaluate(asyncTest);
                    log.info(String.format(str, i, j, evaluation.accuracy(), evaluation.f1(), evaluation.recall(), evaluation.precision()));
                    results[i] = String.format(str, i, j, evaluation.accuracy(), evaluation.f1(), evaluation.recall(), evaluation.precision());

                    asyncTest.reset();
                    asyncTrain.reset();
                }

                //save the nn so we can analyze it later
                File locationToSave = new File("BANN-BCF-NEW-"+i+".zip");
                boolean saveUpdater = true;
                ModelSerializer.writeModel(net, locationToSave, saveUpdater);
            }

            for( String result: results ){
                System.out.println(result);
            }

        } catch(IOException | InterruptedException e) {
            System.out.println("IOEXception");
            System.out.println(e.getMessage());
        } catch(Exception e) {
            System.out.println("Exception e");
            System.out.println(e.getMessage());
        }
    }

    public static void train_new_network(){
        try {
            // ----- Configure the network -----
            MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                    //.seed(123)    //Random number generator seed for improved repeatability. Optional.
                    .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).iterations(1)
                    .weightInit(WeightInit.XAVIER)
                    .updater(Updater.RMSPROP)
                    .learningRate(0.1)
                    //.trainingWorkspaceMode(WorkspaceMode.NONE)
                    //.inferenceWorkspaceMode(WorkspaceMode.NONE)
                    //.lrPolicyDecayRate(1)
                    //.lrPolicyPower(0.5)
                    //.learningRateDecayPolicy(LearningRatePolicy.Inverse)
                    //.gradientNormalization(GradientNormalization.ClipElementWiseAbsoluteValue)
                    //.gradientNormalizationThreshold(0.5)
                    .list()
                    .layer(0, new GravesBidirectionalLSTM.Builder().activation(Activation.TANH).nIn(256).nOut(16).build())
                    .layer(1, new RnnOutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                            .activation(Activation.SOFTMAX).nIn(16).nOut(NUM_LABELS).build())
                    .pretrain(false).backprop(true).build();

            int k = 5;
            int ds_min = 0;
            int ds_max = 10529;
            int ds_size = ds_max - ds_min + 1;
            if (ds_size % k != 0){
                throw new Exception("Data set not divisible by k");
            }
            int kfold_size = ds_size / k;
            int test_lidx = 0;
            int test_ridx = 0;
            int ltrain_lidx = -1;
            int ltrain_ridx = -1;
            int rtrain_lidx = -1;
            int rtrain_ridx = -1;
            String[] results = new String[k];
            // ----- Train the network, evaluating the test set performance at each epoch -----
            String str = "Test set evaluation at kfold %d, epoch %d: Accuracy = %.2f, F1 = %.2f, R = %.2f, P = %.2f";

            for (int i = 0; i < k; i++) {
                MultiLayerNetwork net = new MultiLayerNetwork(conf);
                net.init();
                net.setListeners(new ScoreIterationListener(20));
                //net.setListeners(new PerformanceListener(20));

                // ----- Configure the data (k-fold cross validation) -----
                test_lidx = i * kfold_size;
                test_ridx = test_lidx + kfold_size - 1;

                if (test_lidx - 1 < ds_min) {
                    ltrain_lidx = -1;
                    ltrain_ridx = -1;
                } else {
                    ltrain_lidx = ds_min;
                    ltrain_ridx = test_lidx - 1;
                }

                if (test_ridx + 1 > ds_max) {
                    rtrain_lidx = -1;
                    rtrain_ridx = -1;
                } else {
                    rtrain_lidx = test_ridx + 1;
                    rtrain_ridx = ds_max;
                }

                SequenceRecordReader testFeatures = new CSVSequenceRecordReader();
                testFeatures.initialize(new NumberedFileInputSplit(
                        "experiment/Chimera/ollvm-all/mal/data-low-2lbl-dup-chunks/%d.csv", test_lidx, test_ridx));
                SequenceRecordReader testLabels = new CSVSequenceRecordReader();
                testLabels.initialize(new NumberedFileInputSplit(
                        "experiment/Chimera/ollvm-all/mal/label-low-2lbl-dup-chunks/%d.csv", test_lidx, test_ridx));

                DataSetIterator testData = new SequenceRecordReaderDataSetIterator(testFeatures, testLabels, minibatchSize, NUM_LABELS,
                        false, SequenceRecordReaderDataSetIterator.AlignmentMode.ALIGN_END);

                SequenceRecordReader trainFeatures = new CSVSequenceRecordReader();
                trainFeatures.initialize(new NumberedFileInputNonContiguousSplit(
                        "experiment/Chimera/ollvm-all/mal/data-low-2lbl-dup-chunks/%d.csv", ltrain_lidx, ltrain_ridx, rtrain_lidx, rtrain_ridx));
                SequenceRecordReader trainLabels = new CSVSequenceRecordReader();
                trainLabels.initialize(new NumberedFileInputNonContiguousSplit(
                        "experiment/Chimera/ollvm-all/mal/label-low-2lbl-dup-chunks/%d.csv", ltrain_lidx, ltrain_ridx, rtrain_lidx, rtrain_ridx));

                DataSetIterator trainData = new SequenceRecordReaderDataSetIterator(trainFeatures, trainLabels, minibatchSize, NUM_LABELS,
                        false, SequenceRecordReaderDataSetIterator.AlignmentMode.ALIGN_END);

                System.out.println("Indices:");
                System.out.println(test_lidx + ", " + test_ridx);
                System.out.println(ltrain_lidx + ", " + ltrain_ridx + "; " + rtrain_lidx + ", " + rtrain_ridx);
                System.out.println("----------------------------------------");

                int nEpochs = 6;
                for (int j = 0; j < nEpochs; j++) {
                    System.out.println("Epoch: " + j + "; K-Fold: " + i);
                    net.fit(trainData);

                    //Evaluate on the test set:
                    Evaluation evaluation = net.evaluate(testData);
                    log.info(String.format(str, i, j, evaluation.accuracy(), evaluation.f1(), evaluation.recall(), evaluation.precision()));
                    log.info(evaluation.confusionToString());
                    results[i] = String.format(str, i, j, evaluation.accuracy(), evaluation.f1(), evaluation.recall(), evaluation.precision());

                    testData.reset();
                    trainData.reset();
                }

                //save the nn so we can analyze it later
                File locationToSave = new File("BANN-BCF-NEW-" + i + ".zip");
                boolean saveUpdater = true;
                ModelSerializer.writeModel(net, locationToSave, saveUpdater);
            }

            for( String result: results ){
                System.out.println(result);
            }

        } catch(IOException | InterruptedException e) {
            System.out.println("IOEXception");
            System.out.println(e.getMessage());
        } catch(Exception e) {
            System.out.println("Exception e");
            System.out.println(e.getMessage());
        }
    }
}
