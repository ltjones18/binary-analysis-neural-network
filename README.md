# Binary Analysis Neural Network (BANN) #

This repo is currently in "Working Directory" status meaning it represents the working directory we used when producing our research paper. We will continue commenting, documenting and cleaning up the code as time permits.

This repo can be used for getting a quick start to applying a neural network to a binary analysis problem. Until our paper is accepted, we recommend reading [ Shin et al. ](https://www.usenix.org/system/files/conference/usenixsecurity15/sec15-paper-shin.pdf) to get an idea of a binary analysis neural network.