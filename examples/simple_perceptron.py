import random


class Perceptron:
    weights_in = list()
    threshold = None
    learning_rate = 0.1
    weights_out = list()

    def __init__(self):
        pass

    def initialize(self, n):
        #self.threshold = random.uniform(-0.5, 0.5)
        #self.threshold = 0.2
        self.threshold = -0.1

        for i in xrange(n):
            self.weights_in.append(random.uniform(-0.5, 0.5))
        #self.weights_in.append(0.3)
        #self.weights_in.append(-0.1)

    # takes in xs (list of lists), and ys_act (list)
    def activate(self, xs, ys_act):
        print self.threshold
        if len(xs[0]) != len(self.weights_in):
            print("len(xs[0])=%d" % len(xs[0]))
            print("len(self.weights_in)=%d" % len(self.weights_in))
            raise IndexError

        count = 0
        correct = False
        while not correct and count < 2500:
            count += 1
            correct = True
            for i in xrange(len(xs)):
                x = xs[i]

                sum = 0
                for j in xrange(len(x)):
                    x_j = x[j]
                    w_j = self.weights_in[j]
                    sum += x_j*w_j
                sum -= self.threshold
                sum = round(sum, 5)
                ys_act[i] = step(sum)
                if ys_act[i] != ys_exp[i]:
                    correct = False

                weights_before = list(self.weights_in)
                e = self.training(ys_act[i], ys_exp[i], x)
                weights_after = list(self.weights_in)

                #print ys_act
                print("%d %d %d %f %f %d %d %f %f" %
                      (x[0],x[1],ys_exp[i],weights_before[0],weights_before[1],
                       ys_act[i],e,weights_after[0],weights_after[1]))
            print "============================================"

        print self.threshold

    def training(self, y_act, y_exp, x):
        e = y_exp - y_act
        for i in xrange(len(self.weights_in)):
            self.weights_in[i] += self.learning_rate * x[i] * e

        return e


def step(x):
    return int(x >= 0)

if __name__ == "__main__":
    x_1 = [0, 0]
    x_2 = [0, 1]
    x_3 = [1, 0]
    x_4 = [1, 1]
    xs = list()
    xs.append(x_1)
    xs.append(x_2)
    xs.append(x_3)
    xs.append(x_4)
    ys_exp = [0, 1, 1, 1]
    ys_act = [None, None, None, None]

    p = Perceptron()
    p.initialize(2)
    p.activate(xs, ys_act)