import random
import math


learning_rate = 0.1
debug = False
debug_print = False

#def get_random_wt(F_i):
#    return random.uniform(-2.4/F_i,2.4/F_i)
def get_random_wt(F_i):
    return random.uniform(-1,1)


def sigmoid(x):
    return 1 / (1 + math.exp(-x))


class Neuron:
    threshold = None
    ins = list()
    outs = list()

    def __init__(self, ins, outs):
        self.ins = ins
        self.outs = outs
        if not debug:
            self.threshold = get_random_wt(len(self.ins))
        for _in in self.ins:
            _in.end = self
        for out in self.outs:
            out.start = self
            if not debug:
                out.wt = get_random_wt(len(self.ins))

    def transmit(self,x):
        for out in self.outs:
            out.val = x

    def activate(self):
        sum = 0
        for _in in self.ins:
            sum += _in.val*_in.wt
            #_in.val = None # saving input helps in later stages
        outval = sigmoid(sum - self.threshold)
        if debug_print:
            if w_13 in self.ins:
                print "node 3: %f" % outval
            if w_14 in self.ins:
                print "node 4: %f" % outval
            if w_35 in self.ins:
                print "node 5: %f" % outval
        for out in self.outs:
            out.val = outval

    def start_backprop(self, desired):
        y = self.outs[0].val
        grad = y * (1 - y) * (desired - y)
        if debug_print:
            print "grad_5 = %f" % grad
        for _in in self.ins:
            delta_wt = learning_rate * _in.val * grad
            _in.wt += delta_wt
            _in.grad = grad
            if debug_print:
                if _in is w_35:
                    print "delta-w_35 = %f" % delta_wt
                if _in is w_45:
                    print "delta-w_45 = %f" % delta_wt
        delta_wt = learning_rate * -1 * grad
        self.threshold += delta_wt
        if debug_print:
            print "delta-t_5 = %f" % delta_wt

    def backprop(self):
        y = self.outs[0].val
        sum = 0
        for out in self.outs:
            sum += out.grad * out.wt
        grad = y * (1 - y) * sum
        if debug_print:
            if w_13 in self.ins:
                print "grad_3 = %f" % grad
            if w_14 in self.ins:
                print "grad_4 = %f" % grad
        for _in in self.ins:
            delta_wt = learning_rate * _in.val * grad
            _in.wt += delta_wt
            if debug_print:
                if _in is w_13:
                    print "delta-w_13 = %f" % delta_wt
                if _in is w_23:
                    print "delta-w_23 = %f" % delta_wt
                if _in is w_14:
                    print "delta-w_14 = %f" % delta_wt
                if _in is w_24:
                    print "delta-w_24 = %f" % delta_wt
        delta_wt = learning_rate * -1 * grad
        self.threshold += delta_wt
        if debug_print:
            if w_13 in self.ins:
                print "delta-grad_3 = %f" % delta_wt
            if w_14 in self.ins:
                print "delta-grad_4 = %f" % delta_wt


class Edge:
    start = None
    end = None
    wt = None
    val = None
    grad = None

    def __init__(self, wt=None):
        self.wt = wt


class Layer:
    nodes = list()

    def __init__(self, nodes):
        self.nodes = nodes

    def transmit(self, inputs):
        nodes_l = len(self.nodes)
        inputs_l = len(inputs)
        if nodes_l != inputs_l:
            raise IndexError

        for i in xrange(0,nodes_l):
            self.nodes[i].transmit(inputs[i])

    def activate(self):
        for node in self.nodes:
            node.activate()

    def start_backprop(self, desired):
        for node in self.nodes:
            node.start_backprop(desired)

    def backprop(self):
        for node in self.nodes:
            node.backprop()


# Output the sum of square errors
def feedforward(inputs, desired):
    layer_i.transmit(inputs)
    layer_h.activate()
    layer_o.activate()
    return (desired - w_o.val), w_o.val


def backpropagate(desired):
    layer_o.start_backprop(desired)
    layer_h.backprop()

def print_weights():
    print "w_13 = %f" % w_13.wt
    print "w_14 = %f" % w_14.wt
    print "w_23 = %f" % w_23.wt
    print "w_24 = %f" % w_24.wt
    print "w_35 = %f" % w_35.wt
    print "w_45 = %f" % w_45.wt
    print "t_3 = %f" % n_3.threshold
    print "t_4 = %f" % n_4.threshold
    print "t_5 = %f" % n_5.threshold

if __name__ == "__main__":
    w_01 = Edge()
    w_02 = Edge()
    if not debug:
        w_13 = Edge()
        w_23 = Edge()
        w_14 = Edge()
        w_24 = Edge()
        w_35 = Edge()
        w_45 = Edge()
    else:
        w_13 = Edge(0.5)
        w_23 = Edge(0.4)
        w_14 = Edge(0.9)
        w_24 = Edge(1.0)
        w_35 = Edge(-1.2)
        w_45 = Edge(1.1)
    w_o = Edge()

    n_1 = Neuron([w_01],[w_13, w_14])
    n_2 = Neuron([w_02],[w_23, w_24])
    n_3 = Neuron([w_13, w_23], [w_35])
    n_4 = Neuron([w_14, w_24], [w_45])
    n_5 = Neuron([w_35, w_45], [w_o])

    if debug:
        n_3.threshold = 0.8
        n_4.threshold = -0.1
        n_5.threshold = 0.3

    layer_i = Layer([n_1, n_2])
    layer_h = Layer([n_3, n_4])
    layer_o = Layer([n_5])

    o_w_13 = w_13.wt
    o_w_14 = w_14.wt
    o_w_23 = w_23.wt
    o_w_24 = w_24.wt
    o_w_35 = w_35.wt
    o_w_45 = w_45.wt
    o_n_3 = n_3.threshold
    o_n_4 = n_4.threshold
    o_n_5 = n_5.threshold

    print_weights()
    n = 0
    while n < 250000:
        sum_sq_err = 0
        e, v1 = feedforward([0.1, 0.1], 0.1)
        sum_sq_err += e**2
        backpropagate(0.1)

        e, v2 = feedforward([0.9, 0.1], 0.9)
        sum_sq_err += e**2
        backpropagate(0.9)

        e, v4 = feedforward([0.9, 0.9], 0.1)
        sum_sq_err += e**2
        backpropagate(0.1)

        e, v3 = feedforward([0.1, 0.9], 0.9)
        sum_sq_err += e**2
        backpropagate(0.9)

        if sum_sq_err <= 0.001:
            break
        n += 1
        print n, sum_sq_err, v1, v2, v3, v4
        f = open("error", "w")
        f.write("%d %f" % (n,sum_sq_err))

        if n == 1:
            o_v1 = v1
            o_v2 = v2
            o_v3 = v3
            o_v4 = v4

    print "Original weights-----------"
    print "w_13 = %f" % o_w_13
    print "w_14 = %f" % o_w_14
    print "w_23 = %f" % o_w_23
    print "w_24 = %f" % o_w_24
    print "w_35 = %f" % o_w_35
    print "w_45 = %f" % o_w_45
    print "t_3 = %f" % o_n_3
    print "t_4 = %f" % o_n_4
    print "t_5 = %f" % o_n_5
    print "%f %f %f %f" % (o_v1, o_v2, o_v3, o_v4)
    print "---------------------------"
    print_weights()

    # e,v1 = feedforward([1, 1], 0)
    # print "e = %f" % e
    # backpropagate(0)
    # print_weights()