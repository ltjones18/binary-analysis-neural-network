tic
%close all
clear all

problem = 1;  % 1 = xor,   0 = handwriting

momentumOn = 0;
hiddenMultFactor = 2;
alpha = .1;         %handwriting .1   xor .5
beta  = .9;         %handwriting .8   xor .9
weightScale = 1;    %handwriting .5   xor  1

%% Constant Decs, Import Data


if problem == 0
    input = importdata('inputs.txt');
    desiredOutput = importdata('output.txt');
    input = input';
    desiredOutput = desiredOutput';
    alpha = .1;         %handwriting .1   xor .5
    beta  = .8;         %handwriting .8   xor .9
    weightScale = .5;    %handwriting .5   xor  1
else
    input = [.1 .1; .1 .9; .9 .1; .9 .9]';
    desiredOutput = [.1 .9 .9 .1];
    %input = [0 0; 0 1; 1 0; 1 1]';
    %desiredOutput = [0 1 1 0];
end
numIn = length (input(1,:));

sig = @(x) 1./(1+exp(-x));

%% Scale Network to size of input/outputs and initialize Matricies

numInput = length (input(:,1)) + 1;                %add one for the bias
numHidden = (numInput - 1) * hiddenMultFactor;
%numHidden = 2;
numOutput = length(desiredOutput(:,1));

input = [input; ones(1,numIn)*-1];            %add input for bias of -1

% Initialize weights randomly 
weightsH = rand(numHidden,numInput)*weightScale*2 - weightScale;  %scale from -.1 to .1
weightsO = rand(numOutput,numHidden + 1)*weightScale*2 - weightScale;     %add one for the bias
disp(weightsH)
%weightsH = [.5 .4 .8
%            .9 1 -0.1];
%weightsO = [-1.2 1.1 0.3];

lastDeltaWH = zeros(numHidden,numInput);
lastDeltaWO = zeros(numOutput,numHidden+1); %add one for the bias


MSE = 1;
epoch = 0;

%% Iterate through data to train network
while MSE > .001
   epoch = epoch + 1;
   MSE = 0;
   for j = 1:numIn
       
      % Hidden  and Output layer un Forward
      yH = [sig( weightsH*input(:,j)); -1] ; %add input for bias of -1
      yO = sig(weightsO*yH);

      %yO = sig(weightsO*[sig( weightsH*input(:,8)); -1])
      % Calculate and propagate the delta backwards into hidden layers
      error = desiredOutput(j) - yO;
      deltaO = yO.*(1-yO).*error;
      deltaH = yH(1:end-1).*(1-yH(1:end-1)).*(weightsO(:,1:end-1)'*deltaO);
      
      % Calculate weight and bias changes both layers
      deltaWO = alpha*(deltaO*yH');
      deltaWH = alpha*(deltaH*input(:,j)');

      % Add weight and bias changes to original weights 
      weightsH = weightsH + deltaWH; %+ lastDeltaWH * beta;
      weightsO = weightsO + deltaWO; %+ lastDeltaWO * beta;

      if momentumOn
          lastDeltaWH = deltaWH;
          lastDeltaWO = deltaWO;
      end
      
      %MSE = MSE + sum(error.^2)/numIn;
      MSE = MSE + sum(error.^2);
      
   end  %update each data point
   MSECapture(epoch) = MSE;
end
S = [num2str(epoch),' epochs'];
disp(S);
toc

figure
plot(MSECapture)

% figure
% reshape(input(1:end-1,4),5,6)')
if problem == 0
    figure; hold;
    for image = 1:length(weightsH(:,1))
        showThis = reshape(weightsH(image,1:end-1)',6,5);
        modified = imresize(mat2gray(showThis),15);
        subplot(6,10,image), imshow(modified);
    end
    
    figure; hold;
    for image = 1:length(input(1,:))
        showThis = reshape(input(1:end-1,image),5,6)';
        modified = imresize(mat2gray(showThis),15);
        subplot(6,10,image), imshow(modified);
    end
    
end
% 
% picOne = mat2gray(one)
% picOne = imresize(picOne,20)
% figure
% imshow(picOne)
% 
% features = weightsH.*(weightsH>0);
% for num = 1:30
% feature = reshape(features(num,1:end-1)',5,6)
% figure
% surface (feature)
% end

% 
% features = weightsH.*(weightsH>0);
% addFeatures = zeros(5,6);
% for num = [2 8 9 16 19 26 28]    %1:30
%    addFeatures = addFeatures + reshape(features(num,1:end-1)',5,6);
% end
% 
% figure
% surface (addFeatures)
% 
% close all




